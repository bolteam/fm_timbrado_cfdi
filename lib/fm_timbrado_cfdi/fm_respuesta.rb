# -*- encoding : utf-8 -*-
require 'nokogiri'
require 'savon'
require 'fm_timbrado_cfdi/fm_timbre'

module FmTimbradoCfdi
  class FmRespuesta
    attr_reader :errors, :pdf, :xml, :cbb, :timbre, :no_csd_emisor, :code, :message 
    def initialize(savon_response)
      #inicializamos el estado del objeto
      parse(savon_response)
    end #initialize

    def parse (savon_response)
      #Savon 2.2.0



      @error = false
      @errors = []
      if savon_response.locals && savon_response.locals[:message] && savon_response.locals[:message]["param0"]
        sent_values = savon_response.locals[:message]["param0"]
        @generar_timbre = sent_values["generarTXT"]
        @generar_pdf = sent_values["generarPDF"]
        @generar_cbb = sent_values["generarCBB"]
      else
        @error = true
        @errors << "Respuesta invalida"
      end

      begin
        #vemos si la petción fue correcta
        if savon_response.success?
          #cargamos la respuesta en xml
          @doc = Nokogiri::XML(savon_response.to_xml)

          if @doc.xpath("//ns1:requestTimbrarCFDIResponse").first
            parse_timbrado
          elsif @doc.xpath("//ns1:requestCancelarCFDIResponse").first
            parse_cancelacion
          else
            #errors
          end
        else
          @error = true
          @errors << savon_response.soap_fault.to_s if savon_response.soap_fault.present?
          @pdf = nil
          @xml = nil
          @cbb = nil
          @timbre = nil
          @no_csd_emisor = nil

        end

      rescue Exception => e
        @error = true
        @errors << "No se ha podido realizar el parseo de la respuesta. #{e.message}"
      end
    end #parse
    
    
    
    
    def parse_timbrado
      #Parseamos el nodo xml
      if @doc.xpath("//xml").first
        @xml = Base64::decode64 @doc.xpath("//xml").first.content
        # tratamos de obtener el no de serie del CSD del emisor
        begin
          factura_xml = Nokogiri::XML(@xml)
          @no_csd_emisor = factura_xml.xpath("//cfdi:Comprobante").attribute('noCertificado').value
        rescue Exception => e
          @no_csd_emisor = nil
          @error = true
          @errors << "No se ha podido obtener el CSD del emisor"
        end
      else
        @xml = nil
        @error = true
        @errors << "No se ha encontrado el nodo xml"
      end


      #Parseamos el nodo timbre
      #Si envia parametro de generar timbre. Si no existe en la repuesta marca el error. Si no envia el parametro do nothing
      if @generar_timbre && @doc.xpath("//txt").first
        @timbre = FmTimbre.new Base64::decode64( @doc.xpath("//txt").first.content ) 
      elsif @generar_timbre
        @timbre = nil
        @error = true if @doc.xpath("//txt").empty?
        @errors << "No se ha encontrado el nodo para el timbre fiscal" if @doc.xpath("//txt").empty?
      end

      #Parseamos el nodo pdf
      #similar a txt timbre
      if @generar_pdf && @doc.xpath("//pdf").first
        @pdf = Base64::decode64 @doc.xpath("//pdf").first.content 
      elsif @generar_pdf
        @pdf = nil
        @error = true if @doc.xpath("//pdf").empty?
        @errors << "No se ha encontrado el nodo para el archivo pdf" if @doc.xpath("//pdf").empty?
      end

      #Parseamos el nodo cbb
      #similar a txt timbre
      if @generar_cbb && @doc.xpath("//png").first
        @cbb = Base64::decode64 @doc.xpath("//png")[0].content 
      elsif @generar_cbb
        @cbb = nil
        @error = true if @doc.xpath("//png").empty?
        @errors << "No se ha encontrado el nodo para la imagen cbb" if @doc.xpath("//png").empty? 
      end        
    end
    
    
    def parse_cancelacion
      
      if @doc.xpath("//Code").first
        @code = @doc.xpath("//Code").first.content      
      else
        @code = nil
        @error = true if @doc.xpath("//Code").empty?
        @errors << "No se ha encontrado el nodo para el codigo de cancelacion" if @doc.xpath("//png").empty?
      end
      
      if  @doc.xpath("//Message").first       
        @message = @doc.xpath("//Message").first.content
      else
        @message = nil
        @error = true if @doc.xpath("//Message").empty?
        @errors << "No se ha encontrado el nodo del mensaje" if @doc.xpath("//Message").empty?
      end
    end
    
    

    def valid?
      not @error
    end

    def xml_present?
      not @xml.nil?
    end

    def cbb_present?
      not @cbb.nil?
    end

    def pdf_present?
      not @pdf.nil?
    end

    def timbre_present?
      not @timbre.nil?
    end

    def no_csd_emisor_present?
      not @no_csd_emisor.nil?
    end
  end #class
end #module